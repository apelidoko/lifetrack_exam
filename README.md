
APPLICATION USED
- Wamp Version 3.2.2.2 (PHP7.x)
- Laravel Version 7.0

TO RUN THE APPLICATION
1. Open Command Prompt or GIT Bash
2. CD to Your Project Directory via CMD or GIT Bash
    type: 'git clone https://gitlab.com/apelidoko/lifetrack_exam.git' (On Your Project Directory)
3. Then
    type: 'composer update' // this is to download the default laravel resources
    type: 'composer dump-autoload' // this is to refresh the classes and configuration
4. Open your browser
5. Go to this url: 
    "http://localhost/lifetrack_exam/public/#"  // this will depend on your local setup, ajax url is based on this **lifetrack_exam/public/**
6. Type in the Input Parameters
    - Number of Studies Per Day 
    - Study Growth Percentage
    - Number of Months
7. Click on 'Calculate' button

BRIEF NOTE AND EXPLANATION
1. For the Input Parameters I did not put validation but only positive numbers should be inputted

2. The First Month to be Forecasted is the 1stday of the Next Month But I have considered the growth of Current Month. 

    Please below sample:
    
    $current_study = 1000;
    $growth_per_month = 3%;
    $month_to_forecast = 2; (April to May)
    
    Let's say today is still March, but I want to forecase 2 months.
    * I will apply the growth of the study (3%) for this current month (March) and apply it to the starting of nextmonth.

    * The initial month to forecast will always have additional growth on study before we start 
    because I considered the growth of study on the current month, but I am excluding the cost of the current month since I am only forecasting the next 2 months.
        
        
            $initial_study =  (1000 * 0.03) + 1000; // 1030
            
            loop1 (April)
            {
                $numberOfStudy = ($initial_study * 0.03) + $initial_study; // 1060.9
            }
            
            
            loop2 (May)
            {
                $numberOfStudy = ($numberOfStudy * 0.03) + $numberOfStudy; // 1092.7        
            }
            
            
3. The minimum cost for 2000 study per hour is 0.00553 // as confirmed from email

        scenario1: 
            if I have 1000 study today, it means I will stil have to pay 0.00553 for every hour
            
        scenario2:
            If I have 2001 study today, it means I will have to pay twice the 0.00553 for every hour 
                // because 0.00553 is only for 2000 at max, any excess means I need to add another minimum amount of 0.00553

4. RAM is only consumed by the number of Study per Day, not the commulative number of Study. // as confirmed from email

        scenario1:
            1st day - If I have 1000 number of study on 1st day, the ram will process only 1000;
            2nd day - The number of Study stored becomes 2000, since it gets stored indefinitely, 
                but I will still process 1000 study only per day

5.  I also have added JSON String Result on the Application for reference



Thank You!
    
    
    