<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Main\CostCalculator;


class CostForecastController extends Controller
{
    protected $result;

    public function index()
    {
        return view('pages.index');
    }

    public function generateCost($numberOfStudies, $growth, $numberOfMonths)
    {   
        $forecast = new CostCalculator($numberOfStudies, $growth, $numberOfMonths);
        $forecast->setDateAttributes();
        $forecast->initializeCalculatorConfig();
        $forecast->setCalculatorType();
        $this->result = $forecast->getCostForecast();

        return $this->result;
    }
}
