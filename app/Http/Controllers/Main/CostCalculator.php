<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Http\Controllers\Helpers\FormulaHelper;
use Config;


class CostCalculator extends FormulaHelper
{
    /* Initialize Constant Config Variable */
	protected $config = [];

    /* Input Params */
    protected $currentNumberOfStudy = 0;
    protected $projectedGrowthPerMonth = 0;
    protected $numberOfMonthToForeCast = 0;

	/* Itinialize Other Attributes */
    protected $currentDate;
    protected $startDate;
    protected $finalMonthYear;
    protected $initialNumberOfStudy;
    protected $calculatorType;

    /* Final Data Holder  */
	protected $totalStudyAllMonths = 0;
	protected $totalCostAllMonths = 0;
	protected $monthlyCalculatedListFinal = [];
	protected $lumpSumCalculatedListFinal = [];
	protected $returnedResult = [];


    public function __construct($numberOfStudies, $growth, $numberOfMonths)
    {
    	/* Set Input Params */
    	$this->currentNumberOfStudy = $numberOfStudies;
    	$this->projectedGrowthPerMonth = $growth;
    	$this->numberOfMonthToForeCast = $numberOfMonths;
    }

    /*==================================*/
    /* Get all Configuration Variables */
    /*================================*/
    public function initializeCalculatorConfig()
    {
    	$this->config = Config::get('calculator');
    }


    public function setDateAttributes()
    {
		/* Set Other Attributes */
		$this->currentDate = date('Y-m-d');
		$this->startDate = $this->getFirstDayOfNextMonth();
		$this->initialNumberOfStudy = $this->getStudyGrowth($this->currentNumberOfStudy, $this->projectedGrowthPerMonth); 
		$this->finalMonthYear = $this->getFinalMonthYear($this->numberOfMonthToForeCast, $this->startDate); 
    }


    /*===========================*/
    /* Monthly or Lump Sum Type */
    /*=========================*/
    public function setCalculatorType()
    {
    	if($this->numberOfMonthToForeCast < $this->config['month_break_point'])
    	{
    		$this->calculatorType = $this->config['lump_sum_calculator'];
		}else{
			$this->calculatorType = $this->config['monthly_calculator'];
		}
    }

    /*=====================*/
    /* Calculate the Cost */
    /*===================*/
    public function getCostForecast()
    {
    	/* Initialize Loop Variables */
		$startDateSelectedMonth;
		$yearOfSelectedMonth;
		$monthNameSelectedMonth;
		$monthAndYearOfSelectedMonth;
		$numberOfDaysInSelectedMonth;

		/* Initialize Cost Variables */
		$ramConsumed;
		$ramConsumedCostPerHour;
		$ramConsumedCostPerDay;
		$ramConsumedCostInMonth;
		$totalStoragePerDay;
		$totalStorageInMonth;
		$totalStorageInMonthsCommulative;
		$storageConsumed;
		$storageConsumedCostInMonth;
		$numberOfStudyAllMonthCommulative;

		$numberOfStudySelectedMonth = 0;
		$totalStudySelectedMonth = 0;
		$totalCostSelectedMonth = 0;

		for($i=1;$i<=$this->numberOfMonthToForeCast;$i++)
		{

			/* Get Other Date Variables */
			$iMonth = $i - 1; // nth month to be forecasted - 1 
			$startDateSelectedMonth = $this->getStartDateOfMonth($iMonth, $this->startDate);
			$yearOfSelectedMonth = $this->getYearOfMonth($iMonth, $this->startDate);
			$monthNameSelectedMonth = $this->getMonthNameOfDate($iMonth, $this->startDate); 
			$monthAndYearOfSelectedMonth = $this->appendTwoString($monthNameSelectedMonth, $yearOfSelectedMonth);
			$numberOfDaysInSelectedMonth = date('t', strtotime($startDateSelectedMonth));
			
			/* Compute and Set Initial Number of Study on the First Month */
			if($i == 1)
			{
				$numberOfStudySelectedMonth = $this->getStudyGrowth($this->initialNumberOfStudy, 
		   			$this->projectedGrowthPerMonth); 
			}else{
				$numberOfStudySelectedMonth = $this->getStudyGrowth($numberOfStudySelectedMonth, 
			    	$this->projectedGrowthPerMonth);
			}

			/* Cost Related Variables */
			$totalStudySelectedMonth = $this->getProduct($numberOfDaysInSelectedMonth, 
				$numberOfStudySelectedMonth);
			$ramConsumed = $this->getQuotientAndCeil($numberOfStudySelectedMonth, 
				$this->config['minimum_study']);
			$ramConsumedCostPerHour =  $this->getProduct($ramConsumed, 
				$this->config['minimum_study_ram_cost']);
			$ramConsumedCostPerDay = $ramConsumedCostPerHour * 24; // 24 hours a day
		  	$ramConsumedCostInMonth = $this->getProduct($numberOfDaysInSelectedMonth, 
		  		$ramConsumedCostPerDay);
		  	$totalStoragePerDay = $this->getProduct($numberOfStudySelectedMonth, 
		  		$this->config['storage_per_study']);
			$totalStorageInMonth = $this->getProductAndCeil($totalStoragePerDay, 
				$numberOfDaysInSelectedMonth); 
			
			
			$storageConsumed = $this->getQuotientAndCeil($totalStorageInMonth, 
				$this->config['minimum_storage']); 
			$storageConsumedCostInMonth = $this->getProduct($storageConsumed, 
				$this->config['minimum_storage_cost']); 

			/* Get Commulative Counts of Storage and Study */
			if($i == 1)
			{
				$totalStorageInMonthsCommulative = $storageConsumedCostInMonth; 
				$numberOfStudyAllMonthCommulative = $totalStudySelectedMonth;
			}else{
				$totalStorageInMonthsCommulative = $totalStorageInMonthsCommulative + $storageConsumedCostInMonth;
				$numberOfStudyAllMonthCommulative = $numberOfStudyAllMonthCommulative + $totalStudySelectedMonth;
			}


			$totalCostSelectedMonth = $totalStorageInMonthsCommulative + $ramConsumedCostInMonth;

			/*====================================*/
			/* Push & Construct the Monthly Data */
			/*==================================*/
			array_push($this->monthlyCalculatedListFinal, array(
				'monthAndYearOfSelectedMonth' => $monthAndYearOfSelectedMonth,
				'totalStudySelectedMonth' => $totalStudySelectedMonth,
				'totalCostSelectedMonth' => $totalCostSelectedMonth,
				'totalStudyPerDay' => $numberOfStudySelectedMonth,
				'numberOfStudyAllMonthCommulative' => $numberOfStudyAllMonthCommulative,
				'numberOfDaysInSelectedMonth' => $numberOfDaysInSelectedMonth,
				'ramConsumed' => $ramConsumed,
				'ramConsumedCostPerDay' => $ramConsumedCostPerDay,
				'ramConsumedCostInMonth' => $ramConsumedCostInMonth,
				'totalStoragePerDay' => $totalStoragePerDay,
				'totalStorageInMonth' => $totalStorageInMonth,
				'totalStorageInMonthsCommulative' => $totalStorageInMonthsCommulative,
				'storageConsumed' => $storageConsumed,
				'storageConsumedCostInMonth' => $storageConsumedCostInMonth
			));

		}

		/*=====================*/
		/* Lump SUM Variables */
		/*===================*/

		/* Sum of Total Studies All Months */
		$this->totalStudyAllMonths = $this->sumArrayKeyValue(
			$this->monthlyCalculatedListFinal, 
			'totalStudySelectedMonth'
		); 

		/* Sum of Cost All Months */
		$this->totalCostAllMonths = $this->sumArrayKeyValue(
			$this->monthlyCalculatedListFinal, 
			'totalCostSelectedMonth'
		); 

		/* Push & Construct the Lump Sum Data Structure */
		array_push($this->monthlyCalculatedListFinal, array(
			'monthAndYearOfSelectedMonth' => $this->finalMonthYear,
			'totalStudySelectedMonth' => $this->totalStudyAllMonths,
			'totalCostSelectedMonth' => $this->totalCostAllMonths,
			'numberOfStudyAllMonthCommulative' => $numberOfStudyAllMonthCommulative,
		));

		return $this->returnOutputBasedOnFormat($this->calculatorType);
    }

    /*=====================*/
    /* Return Result Data */
    /*===================*/
    public function returnOutputBasedOnFormat($calculatorType)
    {
    	Switch($calculatorType){

			case $this->config['monthly_calculator']:
				/* Retain Array for Monthly Data Only */
				array_pop($this->monthlyCalculatedListFinal);
				$this->returnedResult = $this->monthlyCalculatedListFinal;
				break;

			default:
				/* Retain Array for Lump Sum Data Only */
				array_push($this->lumpSumCalculatedListFinal, 
					end($this->monthlyCalculatedListFinal)
				);
				$this->returnedResult = $this->lumpSumCalculatedListFinal;
		}
		
		return $this->returnedResult;
    }

}
