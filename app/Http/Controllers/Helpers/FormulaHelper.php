<?php

namespace App\Http\Controllers\Helpers;


class FormulaHelper 
{

    public function getStudyGrowth($numberOfStudy,$projectedGrowth)
    {
        return $numberOfStudy * ($projectedGrowth/100) + $numberOfStudy;
    }


    public function getFirstDayOfNextMonth() 
    {
        return date('Y-m-d', strtotime('first day of next month'));
    }


    public function getFinalMonthYear($numberOfMonthsForecast,$startDate) 
    {
        return date('M Y', strtotime("-1 months",strtotime("+$numberOfMonthsForecast months", strtotime($startDate))));
    }


    public function getStartDateOfMonth($numnberOfMonths, $startDate)
    {
        return date('Y-m-d', strtotime("+$numnberOfMonths months", strtotime($startDate)));
    }


    public function getYearOfMonth($numnberOfMonths, $date)
    {
        return date('Y', strtotime("+$numnberOfMonths months", strtotime($date)));
    }


    public function getMonthNameOfDate($numnberOfMonths, $date)
    {
        return date('M', strtotime("+$numnberOfMonths months", strtotime($date)));
    }


    public function appendTwoString($stringA, $StringB)
    {
        return $stringA . " " . $StringB;
    }

    public function getProduct($value1, $value2)
    {
        return $value1 * $value2;
    }


    public function getProductAndCeil($value1, $value2)
    {
        return ceil($value1 * $value2);
    }


    public function getQuotient($value1, $value2)
    {
        return $value1 / $value2;
    }


    public function getQuotientAndCeil($value1, $value2)
    {
        return ceil($value1 / $value2);
    }


    public function sumArrayKeyValue($array,$arrayKey)
    {
        return array_sum(array_column($array, $arrayKey)); 
    }

    
}
