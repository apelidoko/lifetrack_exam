<?php

use Illuminate\Support\Facades\Route;


Route::get('/', 'Main\CostForecastController@index');

Route::resource('lifetrack', 'Main\CostForecastController');

Route::get('/lifetrack/{numberOfStudies}/{growth}/{numberOfMonths}', 
	'Main\CostForecastController@generateCost')->name('show.post');

