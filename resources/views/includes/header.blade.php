<div class="collapse bg-dark" id="navbarHeader"></div>

<div class="navbar navbar-dark bg-dark box-shadow col col-sm-12">
	
	<div class="container d-flex justify-content-between">
		
		<a href="#" class="navbar-brand d-flex align-items-center">
			<strong><span>Lifetrack Cost Forecaster</span></strong>
		</a>
		
		<button class="navbar-toggler" type="button">
			
			<span class="navbar-toggler-icon"></span>

		</button>

	</div>

</div>