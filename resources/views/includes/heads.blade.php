<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Lifetrack Cost Forecaster</title>

<!-- Bootstrap core CSS -->
<link href="{{ asset('vendors/bootstrap/bootstrap.min.css') }}" rel="stylesheet" />
<!--- Custom CSS --->
<link href="{{ asset('css/custom.css') }}" rel="stylesheet" />