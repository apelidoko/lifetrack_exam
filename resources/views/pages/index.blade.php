@extends('layout.default')
@section('content')

<div class="container">

	<div class="row justify-content-center">

		<div class="col-12 col-md-8 col-lg-6 pb-5">

			<form id="calculateCostForm" action="" method="post">

				<div class="card rounded-0 mt-5 calculator-form-container">

					<div class="calculator-card-heading card-header p-0">

						<div class="text-white text-center py-2">

							<h5><span style='text-shadow:2px 1px #333;'>Infrastructure Cost Calculator</span></h5>

						</div>

					</div>

					<div class="card-body p-3">

						<div class="form-group">

							<div class="input-group mb-2">
								
								<input type="text" class="form-control" id="numberOfStudiesPerDay" name="numberOfStudiesPerDay" placeholder="Number of Studies per Day" pattern="(\d)" required>

							</div>

						</div>

						<div class="form-group">

							<div class="input-group mb-2">

								<input type="text" class="form-control" id="studyGrowthPercentage" name="studyGrowthPercentage" placeholder="Study Growth Percentage" required>

							</div>

						</div>

						<div class="form-group">

							<div class="input-group mb-2">

								<input type="text" class="form-control" id="numberOfMonths" name="numberOfMonths" placeholder="Number of Months" required>

								<input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}">

							</div>

						</div>

						<div class="text-center">

							<button type="button" id="calculateCostBtn" class="btn btn-danger btn-block rounded-0 py-2">Calculate</button>
						
						</div>

					</div>

				</div>

			</form>

		</div>

	</div>

	<div class="row justify-content-center">

		<div class="col-12 col-md-8 col-lg-6 pb-5">

			<div class="card rounded-0 calculator-form-container">

				<div class="calculator-card-heading card-header p-0">

					<div class="text-white text-center py-2">

						<h5><span style='text-shadow:2px 1px #333;'>Result</span></h5>

					</div>

				</div>

				<div class="card-body p-6">

					<table class="table table-hover" id="cost-table">

						<thead>

							<tr>

								<th>Month Year</th>

								<th>Number of Studies</th>

								<th>Cost Forecasted</th>

							</tr>

						</thead>

						<tbody>
							
						</tbody>

					</table>

				</div>

			</div>

		</div>

	</div>

	<div class="row justify-content-center">

		<div class="col-12 col-md-8 col-lg-6 pb-5">

			<div class="card rounded-0 calculator-form-container">

				<div class="calculator-card-heading card-header p-0">

					<div class="text-white text-center py-2">

						<h5><span style='text-shadow:2px 1px #333;'>JSON String Result</span></h5>

					</div>

				</div>

				<div id="json-result" class="card-body p-6">

				</div>

			</div>

		</div>

	</div>

</div>



@stop