<!doctype html>
<html>

    <head>

        @include('includes.heads')

    </head>

    <body>

        <div class="container-fluid">

            <header class="row">

                @include('includes.header')

            </header>

            <div id="main" class="row main-container">

                @yield('content')

            </div>

        </div> 

    </body>

    @include('includes.footer')

</html>