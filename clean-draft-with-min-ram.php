<?


$currentNumberOfStudy = 1000;
$projectedGrowthPerMonth = 3;
$numberOfMonthToForeCast = 1;
$ramCostPerStudy = 0.00006636;
$storageCostPerStudy = 0.001;
$currentDate = date('Y-m-d');
$startDate = date('Y-m-d', strtotime('first day of next month'));
$endDate = date('Y-m-d', strtotime("+$numberOfMonthToForeCast months", strtotime($startDate)));
$endDate = date('Y-m-t', strtotime($endDate));
$initialNumberOfStudy = $currentNumberOfStudy * ($projectedGrowthPerMonth/100) + $currentNumberOfStudy;

$startDateSelectedMonth;
$monthNumberSelectedMonth;
$yearOfSelectedMonth;
$monthNameSelectedMonth;
$numberOfDaysInSelectedMonth;
$numberOfStudySelectedMonth = 0;

$ramConsumedCostPerDay = 0;
$ramConsumedCostInMonth = 0;
$totalStudySelectedMonth = 0;
$storageConsumedCostInMonth = 0;
$totalStorageInMonth = 0;


/*================================*/

/* Initialize Other Constant Variables */
$minimumStudy;
$minimumStudyRamCost;
$storagePerStudy;
$minimumStorage;
$minimumStorageCost;

/* Constants */
$minimumStudy = 2000;
$minimumStudyRamCost = 0.00553;
$storagePerStudy = 10;
$minimumStorage = 1000;
$minimumStorageCost = 0.10;


/* Initialize Cost Variables */
$ramConsumed;
$ramConsumedCostPerHour;
$ramConsumedCostPerDay;
$ramConsumedCostInMonth;
$totalStoragePerDay;
$totalStorageInMonth;
$storageConsumed;
$storageConsumedCostInMonth;


for($i=1;$i<=$numberOfMonthToForeCast;$i++)
{

	$iMonth = $i - 1; // nth month to be forecasted - 1 
	$startDateSelectedMonth = date('Y-m-d', strtotime("+$iMonth months", strtotime($startDate)));
	$monthNumberSelectedMonth = date('m', strtotime("+$iMonth months", strtotime($startDate)));
	$yearOfSelectedMonth = date('Y', strtotime("+$iMonth months", strtotime($startDate)));
	$monthNameSelectedMonth = date('M', strtotime("+$iMonth months", strtotime($startDate)));
	$numberOfDaysInSelectedMonth = date('t', strtotime($startDateSelectedMonth));
	
	// Compute and Set Initial Number of Study on the First Month
	if($i == 1)
	{
	   $numberOfStudySelectedMonth = $initialNumberOfStudy * ($projectedGrowthPerMonth/100) + $initialNumberOfStudy;
	}else{
	   $numberOfStudySelectedMonth = $numberOfStudySelectedMonth * ($projectedGrowthPerMonth/100) + $numberOfStudySelectedMonth;
	}

	/* Cost Related Variables */
	$ramConsumed = ceil($numberOfStudySelectedMonth / $minimumStudy);
	$ramConsumedCostPerHour =  ($ramConsumed * $minimumStudyRamCost);
	$ramConsumedCostPerDay = $ramConsumedCostPerHour * 24;
  	$ramConsumedCostInMonth = $numberOfDaysInSelectedMonth * $ramConsumedCostPerDay;
	$totalStudySelectedMonth = $numberOfDaysInSelectedMonth * $numberOfStudySelectedMonth;
	$totalStoragePerDay = $numberOfStudySelectedMonth * $storagePerStudy;
	$totalStorageInMonth = ceil($totalStoragePerDay * $numberOfDaysInSelectedMonth);
	$storageConsumed = ceil($totalStorageInMonth / $minimumStorage);
	$storageConsumedCostInMonth = $storageConsumed * $minimumStorageCost;

  
  	echo "<br>";
	echo "<br>";
	echo "numberOfStudySelectedMonth: " . $numberOfStudySelectedMonth;
	echo "<br>";
	echo "totalStudySelectedMonth: " . $totalStudySelectedMonth;
	echo "<br>";
	echo "startDateSelectedMonth: " . $startDateSelectedMonth;
	echo "<br>";
	echo "numberOfDaysInSelectedMonth: " . $numberOfDaysInSelectedMonth;
	echo "<br>";
	echo "monthNameSelectedMonth: " . $monthNameSelectedMonth;
	echo "<br>";
	echo "monthNumberSelectedMonth: " . $monthNumberSelectedMonth;
	echo "<br>";
	echo "yearOfSelectedMonth: " . $yearOfSelectedMonth;
	echo "<br>";
	echo "ramConsumed: " . $ramConsumed;
	echo "<br>";
	echo "ramConsumedCostPerHour: " . $ramConsumedCostPerHour;
	echo "<br>";
	echo "ramConsumedCostPerDay: " . $ramConsumedCostPerDay;
	echo "<br>";
	echo "ramConsumedCostInMonth: " . $ramConsumedCostInMonth;
	echo "<br>";
	echo "totalStoragePerDay: " . $totalStoragePerDay;
	echo "<br>";
	echo "totalStorageInMonth: " . $totalStorageInMonth;
	echo "<br>";
	echo "storageConsumedCostInMonth: " . $storageConsumedCostInMonth;
	echo "<br>";
	echo "<br>";
	echo "<br>";

}
