<?php 

$currentNumberOfStudy = 1000;
$projectedGrowthPerMonth = 3;
$numberOfMonthToForeCast = 13;
$ramCostPerStudy = 0.00006636;
$storageCostPerStudy = 0.001;
$currentDate = date('Y-m-d');
$startDate = date('Y-m-d', strtotime('first day of next month'));
$endDate = date('Y-m-d', strtotime("+$numberOfMonthToForeCast months", strtotime($startDate)));
$endDate = date('Y-m-t', strtotime($endDate));
$initialNumberOfStudy = $currentNumberOfStudy * ($projectedGrowthPerMonth/100) + $currentNumberOfStudy;


echo "<br>";
echo "start date: " . $startDate;
echo "<br>";
echo "endDate: " . $endDate;
echo "<br>";
echo "initial number of study: " . $initialNumberOfStudy;
echo "<br>";
echo "cost per study: " . $ramCostPerStudy;
echo "<br>";
echo "-------Start-----------";
echo "<br>";
echo "<br>";

$current_study_cummulative = 0;

for($i=1;$i<=$numberOfMonthToForeCast;$i++)
{

	$iMonth = $i - 1; // nth month to be forecasted - 1

	$selected_date = date('Y-m-d', 
		strtotime("+$iMonth months", 
			  strtotime($startDate)));
	
	$selected_date_month_number = date('m', 
		strtotime("+$iMonth months", 
			  strtotime($startDate)));
	
	$selected_date_year = date('Y', 
		strtotime("+$iMonth months", 
			  strtotime($startDate)));
	
	$selected_month_name = date('M', 
		strtotime("+$iMonth months", 
			  strtotime($startDate)));
	
	$number_of_days_in_selected_month = date('t', 
		strtotime($selected_date));
	
	
	if($i == 1)
	{
	   $current_study_cummulative = $initialNumberOfStudy * ($projectedGrowthPerMonth/100) + $initialNumberOfStudy;
	}else{
	   $current_study_cummulative = $current_study_cummulative * ($projectedGrowthPerMonth/100) + $current_study_cummulative;
	}

	$ram_cost_per_day = $current_study_cummulative * $ramCostPerStudy;
  
	$ram_cost_this_month = $ram_cost_per_day * $number_of_days_in_selected_month;

$total_study_at_end_of_month = $number_of_days_in_selected_month * $current_study_cummulative;
  
$storage_cost_this_month = $total_study_at_end_of_month * $storageCostPerStudy;
  
	echo "<br>";
	echo "selected month number of study: " . $current_study_cummulative;
		echo "<br>";
echo "total study at end of month: " . $total_study_at_end_of_month;
	echo "<br>";
		echo "storage cost this month: " . $storage_cost_this_month;
	echo "<br>";
	echo "selected date: " . $selected_date;
	echo "<br>";
	echo "number of days of selected date: " . $number_of_days_in_selected_month;
	echo "<br>";
	echo "month name: " . $selected_month_name;
	echo "<br>";
	echo "month: " . $selected_date_month_number;
	echo "<br>";
	echo "year: " . $selected_date_year;
	echo "<br>";
	echo "ram cost per day: " . $ram_cost_per_day;
	echo "<br>";
	echo "ram cost this month: " . $ram_cost_this_month;
	echo "<br>";
	
}