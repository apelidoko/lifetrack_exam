<?php

return [

    'ram_cost_per_study' => 0.00006636,

    'storage_cost_per_study' => 0.001,

    'month_break_point' => 12,

	'lump_sum_calculator' => "LUMP_SUM_CALCULATOR",    

	'monthly_calculator' => "MONTHLY_CALCULATOR",   

	'minimum_study' => 2000,

	'minimum_study_ram_cost' => 0.00553,

	'storage_per_study' => 10,

	'minimum_storage' => 1000,

	'minimum_storage_cost' => 0.10

];
